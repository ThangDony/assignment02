// Thang Dony Le | 2135073
package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args) {

        Bicycle[] bicycles = new Bicycle[4];

        bicycles[0] = new Bicycle("Specialized", 21, 40.0);
        bicycles[1] = new Bicycle("Trek", 18, 35.5);
        bicycles[2] = new Bicycle("Giant", 24, 45.2);
        bicycles[3] = new Bicycle("Cannondale", 20, 38.7);
        
        for(Bicycle bicycle : bicycles) {
            System.out.println(bicycle);
        }

    }
}
